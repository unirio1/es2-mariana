package com.example.Externo.models;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Data
@AllArgsConstructor

public class Cobranca {
    private Integer id;
    private String status;
    private LocalDateTime horaSolicitacao;
    private LocalDateTime horaFinalizacao;
    private double valor;
    private Integer ciclista;

    public void setId(int i) {
    }

    public void setHoraSolicitadacao(LocalDateTime now) {
    }

    public Object getId() {
        return null;
    }

    public double getValor() {
        return valor;
    }

    public Integer getCiclista() {
        return null;
    }

    public Object getStatus(){return status; };

    public void setStatus(String processada) {
    }

    public void setHoraSolicitacao(LocalDateTime now) {
    }

    public String getHoraSolicitacao() {

        return getHoraSolicitacao();
    }

    public Cartao getCartao() {
        return getCartao();
    }
}
