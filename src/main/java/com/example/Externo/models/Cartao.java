package com.example.Externo.models;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cartao {


    @NotNull(message = "O atributo 'nomeTitular' não pode ser nulo")
    private String nomeTitular;

    @NotNull(message = "O atributo 'numero' não pode ser nulo")
    private String numero;

    @NotNull(message = "O atributo 'validade' não pode ser nulo")
    private String validade;

    @NotNull(message = "O atributo 'cvv' não pode ser nulo")
    private String cvv;

    //sequencia dos numeros do cartao (xxxx xxxx---)
    public CharSequence getNumero() {

        return null;
    }

    public Object getNomeTitular() {
        return null;
    }

    public String getValidade() {
        return null;
    }

    public Object getCvv() {
        return null;
    }
}
